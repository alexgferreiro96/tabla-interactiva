import React, { useState, useEffect } from 'react';
import { Button, Col, Container, Row } from 'reactstrap';

export default (props) => {

    const columnas = props.columnas.map((col,index) => {
        return <Col key={index+"-col"}><Button color={col.mostrar ? "warning":"secondary"} onClick={()=>props.cambiaColumna(col)}>{col.nom}</Button></Col>
    });

    return(
        <Container fluid>
            <Row>
                {columnas}
            </Row>
        </Container>
    );
};
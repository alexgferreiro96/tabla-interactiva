import React from "react";
import { Table, Container, Row, Col, Button } from "reactstrap";

export default (props) => {
   
    const columnas = props.columnas.filter(col => col["mostrar"] === true).map(col => (
        <th key={col["posicio"]}><Button color={"primary"} onClick={()=>props.ordenar(col)}>{col["nom"]}</Button></th>
    ));

    const filas = props.datos.map( el => (
            <tr key={el.id}>
                {props.columnas.filter(col => col["mostrar"] === true).map( (data,index) => {
                     return <td key={el.id+"-td-"+index}>{el[data.nom]}</td>
                })}
                <td><button onClick={() => props.borra(el.id)}>borrar</button></td>
                <td><button onClick={() => props.muestra(el.id)}>mostrar</button></td>
            </tr>
        )
    );

  return (
    <>
      <Container>
        <Table striped>
          <thead>
            <tr>
              {columnas}
            </tr>
          </thead>
          <tbody>
           {filas}
          </tbody>
          <tfoot></tfoot>
        </Table>
      </Container>
    </>
  );
};

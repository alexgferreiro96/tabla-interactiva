import React, { useState, useEffect } from 'react';
import Taula from './Taula.jsx';
import 'bootstrap/dist/css/bootstrap.min.css';
import  MOTOS  from './motos.json';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Form, FormGroup } from 'reactstrap';
import SeleccionarColumnas from './SeleccionarColumnas.jsx';


function App() {
  let idx = 0;
  let datos2 = MOTOS.map(el => {
    idx = idx + 1;
    el.id = idx;
    return el;
  });
  
  let columnas = Object.getOwnPropertyNames(MOTOS[0]);

  let  arrColumnas = [];

  for (let index = 0; index < columnas.length; index++) {
    arrColumnas.push({
      "posicio": index+1,
      "nom": columnas[index],
      "tipus": typeof MOTOS[0][columnas[index]],
      "mostrar": true
    });  
  }

  const [datosConId, setDatosConId] = useState(datos2);
  const [laMoto, setLaMoto] = useState(MOTOS[0]);
  const [modal, setModal] = useState(false);
  const [datosColumnas, setDatosColumnas] = useState(arrColumnas);
  const toggle = () => setModal(!modal);

  const ordenarPor = (col) => {
    const datosOrdenados = [...datosConId];    
    if(col.tipus === "number"){
      datosOrdenados.sort(((a, b) => b[col.nom] - a[col.nom]));
      setDatosConId(datosOrdenados);
    }else{
      datosOrdenados.sort(((a, b) => a[col.nom] > b[col.nom]));
      setDatosConId(datosOrdenados);
    }
  }

  const borraItem = (x) => {
    setDatosConId(datosConId.filter(el => el.id !== x));
  }

  const muestraItem = (x) => {
    setLaMoto(datosConId.find(el => el.id === x));
    toggle();
  }

  const cambiaCol = (colum) => {
    const auxColumnas = [...datosColumnas];
    auxColumnas.map((col) => {
      if(col.nom === colum.nom && col.posicio === colum.posicio) col.mostrar = !col.mostrar;
    });
    setDatosColumnas(auxColumnas);
  }

  return (
    <>
      <SeleccionarColumnas columnas={datosColumnas} cambiaColumna={cambiaCol} />
      <Taula datos={datosConId} columnas={datosColumnas} borra={borraItem} muestra={muestraItem} ordenar={ordenarPor} />

      <Modal isOpen={modal} toggle={toggle} >
        <ModalHeader toggle={toggle}>{laMoto.model}</ModalHeader>
        <ModalBody>
          {Object.values(laMoto).forEach(el => {
            console.log(el);
            return (<div>{el}</div>)
          })}
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>

    </>
  );
}

export default App;
